Stanford Algorithms Part 3
==========================

This repo contains solutions to the programming assignments of the course
"Greedy Algorithms, Minimum Spanning Trees, and Dynamic Programming". This
course is offered on [Coursera](https://www.coursera.org/learn/algorithms-greedy)
by Stanford University.

All the solutions were implemented in Rust. In fact, I decided to take this
course to become a better Rustacean. So don't expect expert Rust code. My
code is pretty decent though.

This repo is organized as follows. For every week, I created a library crate.
These crates contain the actual assignment code. I also created a binary crate
named "answers". As the name suggests, this crate can be used to obtain the
assignment answers. To do so, clone this repo and in its root directory use the
following command:
```
cargo run -r
```

If you just want to check out my solutions, you can follow one of the links below:
- Week 1
  - [Greedy job scheduling](https://gitlab.com/mwoitek/stanford-algorithms-3/-/blob/master/week_1/src/jobs.rs)
  - [Prim's algorithm](https://gitlab.com/mwoitek/stanford-algorithms-3/-/blob/master/week_1/src/prim.rs)
- Week 2
  - Union-find implementation
  - Kruskal's algorithm
- Week 3
  - [Huffman coding](https://gitlab.com/mwoitek/stanford-algorithms-3/-/blob/master/week_3/src/huffman.rs)
  - [Maximum-weight independent sets](https://gitlab.com/mwoitek/stanford-algorithms-3/-/blob/master/week_3/src/sets.rs)
- Week 4
  - [0-1 Knapsack problem](https://gitlab.com/mwoitek/stanford-algorithms-3/-/blob/master/week_4/src/lib.rs)
