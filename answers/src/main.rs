use week_1::jobs::{weighted_sum_diff, weighted_sum_ratio};
use week_1::prim;
use week_3::huffman::max_min_lengths;
use week_3::sets::check_membership;
use week_4::{knapsack, knapsack_optimized};

fn main() {
    println!("Programming Assignments: Answers\n");
    assignment_1();
    // println!();
    // assignment_2();
    println!();
    assignment_3();
    println!();
    assignment_4();
}

fn assignment_1() {
    println!("Assignment 1");

    let path_1 = "week_1/text_files/jobs.txt";
    match weighted_sum_diff(path_1) {
        Ok(answer) => {
            println!("Answer 1: {}", answer); // 69119377652
        }
        Err(err) => {
            println!("{}", err);
        }
    }

    match weighted_sum_ratio(path_1) {
        Ok(answer) => {
            println!("Answer 2: {}", answer); // 67311454237
        }
        Err(err) => {
            println!("{}", err);
        }
    }

    let path_2 = "week_1/text_files/edges.txt";
    match prim::Graph::from_file(path_2) {
        Ok(graph) => {
            let start = 1;
            let (_, cost) = graph.prim(start);
            println!("Answer 3: {}", cost); // -3612829
        }
        Err(err) => {
            println!("{}", err);
        }
    }
}

fn assignment_3() {
    println!("Assignment 3");

    let path_1 = "week_3/text_files/huffman.txt";
    match max_min_lengths(path_1) {
        Ok((max_length, min_length)) => {
            println!("Answer 1: {}", max_length); // 19
            println!("Answer 2: {}", min_length); // 9
        }
        Err(err) => {
            println!("{}", err);
        }
    }

    let path_2 = "week_3/text_files/mwis.txt";
    let vertices = [1, 2, 3, 4, 17, 117, 517, 997];
    match check_membership(path_2, &vertices) {
        Ok(answer) => {
            println!("Answer 3: {}", answer); // 10100110
        }
        Err(err) => {
            println!("{}", err);
        }
    }
}

fn assignment_4() {
    println!("Assignment 4");

    let path_1 = "week_4/text_files/knapsack1.txt";
    match week_4::read_input(path_1) {
        Ok((items, capacity)) => {
            let max_val = knapsack(&items, capacity);
            println!("Answer 1: {}", max_val); // 2493893
        }
        Err(err) => {
            println!("{}", err);
        }
    }

    let path_2 = "week_4/text_files/knapsack_big.txt";
    match week_4::read_input(path_2) {
        Ok((items, capacity)) => {
            let max_val = knapsack_optimized(&items, capacity);
            println!("Answer 2: {}", max_val); // 4243395
        }
        Err(err) => {
            println!("{}", err);
        }
    }
}
