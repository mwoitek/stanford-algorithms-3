//! Week 1: Programming Assignment
//!
//! Greedy job scheduling

use std::cmp::Ordering;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

type JobPair = (u64, u64);

// Helper function to read job weights and lengths.
fn parse_line(line: &str, line_num: usize) -> Result<JobPair, String> {
    let mut parts = line.split_whitespace();

    let weight_str = parts.next().ok_or(format!("Could not read weight in line {}", line_num))?;
    let length_str = parts.next().ok_or(format!("Could not read length in line {}", line_num))?;

    let weight: u64 = weight_str
        .parse()
        .map_err(|e| format!("Could not parse weight in line {}: {}", line_num, e))?;
    let length: u64 = length_str
        .parse()
        .map_err(|e| format!("Could not parse length in line {}: {}", line_num, e))?;

    Ok((weight, length))
}

// Function for reading input files containing job data.
fn read_jobs<P: AsRef<Path>>(path: P) -> Result<Vec<JobPair>, String> {
    let file = File::open(path).map_err(|e| format!("Could not open file: {}", e))?;
    let mut lines = BufReader::new(file).lines().map(|l| l.unwrap());

    let first_line = lines.next().ok_or("File is empty".to_owned())?;
    let num_jobs: usize =
        first_line.trim().parse().map_err(|e| format!("Could not parse number of jobs: {}", e))?;

    let mut jobs: Vec<JobPair> = Vec::with_capacity(num_jobs);

    for (i, line) in lines.enumerate() {
        let pair = parse_line(&line, i + 2)?;
        jobs.push(pair);
    }

    Ok(jobs)
}

// Data type that represents a job when difference scoring is used.
#[derive(Debug, Eq, PartialEq)]
struct JobDiff {
    weight: u64,
    length: u64,
    score: i64,
}

impl JobDiff {
    pub fn new(pair: &JobPair) -> Self {
        Self { weight: pair.0, length: pair.1, score: pair.0 as i64 - pair.1 as i64 }
    }
}

// The following traits need to be implemented, since later we sort a Vec<JobDiff>.
impl Ord for JobDiff {
    fn cmp(&self, other: &Self) -> Ordering {
        let ord_score = other.score.cmp(&self.score);
        match ord_score {
            Ordering::Equal => other.weight.cmp(&self.weight),
            _ => ord_score,
        }
    }
}

impl PartialOrd for JobDiff {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// Function to compute the weighted sum of completion times when difference scoring is used.
pub fn weighted_sum_diff<P: AsRef<Path>>(path: P) -> Result<u64, String> {
    let pairs = read_jobs(path)?;

    let mut jobs: Vec<_> = pairs.iter().map(JobDiff::new).collect();
    jobs.sort_unstable();

    let mut time_sum = 0;
    let mut weighted_sum = 0;

    for job in jobs {
        time_sum += job.length;
        weighted_sum += job.weight * time_sum;
    }

    Ok(weighted_sum)
}

// Data type that represents a job when ratio scoring is used.
#[derive(Debug, PartialEq)]
struct JobRatio {
    weight: u64,
    length: u64,
    score: f64,
}

impl JobRatio {
    pub fn new(pair: &JobPair) -> Self {
        Self { weight: pair.0, length: pair.1, score: pair.0 as f64 / pair.1 as f64 }
    }
}

impl PartialOrd for JobRatio {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let ord_score = other.score.partial_cmp(&self.score);
        match ord_score {
            Some(Ordering::Equal) => Some(other.weight.cmp(&self.weight)),
            _ => ord_score,
        }
    }
}

/// Function to compute the weighted sum of completion times when ratio scoring is used.
pub fn weighted_sum_ratio<P: AsRef<Path>>(path: P) -> Result<u64, String> {
    let pairs = read_jobs(path)?;

    let mut jobs: Vec<_> = pairs.iter().map(JobRatio::new).collect();
    jobs.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());

    let mut time_sum = 0;
    let mut weighted_sum = 0;

    for job in jobs {
        time_sum += job.length;
        weighted_sum += job.weight * time_sum;
    }

    Ok(weighted_sum)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_diff_scoring_10_jobs() {
        let path = "text_files/input_random_3_10.txt";
        let result = weighted_sum_diff(path);
        assert_eq!(result, Ok(58550));
    }

    #[test]
    fn test_diff_scoring_20_jobs() {
        let path = "text_files/input_random_7_20.txt";
        let result = weighted_sum_diff(path);
        assert_eq!(result, Ok(235205));
    }

    #[test]
    fn test_diff_scoring_40_jobs() {
        let path = "text_files/input_random_10_40.txt";
        let result = weighted_sum_diff(path);
        assert_eq!(result, Ok(1175612));
    }

    #[test]
    fn test_diff_scoring_80_jobs() {
        let path = "text_files/input_random_13_80.txt";
        let result = weighted_sum_diff(path);
        assert_eq!(result, Ok(4701768));
    }

    #[test]
    fn test_diff_scoring_160_jobs() {
        let path = "text_files/input_random_19_160.txt";
        let result = weighted_sum_diff(path);
        assert_eq!(result, Ok(16615956));
    }

    #[test]
    fn test_ratio_scoring_10_jobs() {
        let path = "text_files/input_random_3_10.txt";
        let result = weighted_sum_ratio(path);
        assert_eq!(result, Ok(57262));
    }

    #[test]
    fn test_ratio_scoring_20_jobs() {
        let path = "text_files/input_random_7_20.txt";
        let result = weighted_sum_ratio(path);
        assert_eq!(result, Ok(222991));
    }

    #[test]
    fn test_ratio_scoring_40_jobs() {
        let path = "text_files/input_random_10_40.txt";
        let result = weighted_sum_ratio(path);
        assert_eq!(result, Ok(1142691));
    }

    #[test]
    fn test_ratio_scoring_80_jobs() {
        let path = "text_files/input_random_13_80.txt";
        let result = weighted_sum_ratio(path);
        assert_eq!(result, Ok(4595737));
    }

    #[test]
    fn test_ratio_scoring_160_jobs() {
        let path = "text_files/input_random_19_160.txt";
        let result = weighted_sum_ratio(path);
        assert_eq!(result, Ok(16187388));
    }
}
