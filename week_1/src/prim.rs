//! Week 1: Programming Assignment
//!
//! Implementation of Prim's algorithm

use anyhow::{anyhow, Result};
use rustc_hash::{FxHashMap, FxHashSet};
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

type Node = u32;
type Cost = i32;

// Data type that represents an edge of the graph.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Edge {
    tail: Node,
    head: Node,
    cost: Cost,
}

// Edges will be added to a BinaryHeap. Then it's necessary to implement Ord.
impl Ord for Edge {
    fn cmp(&self, other: &Self) -> Ordering {
        other.cost.cmp(&self.cost)
    }
}

impl PartialOrd for Edge {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

// Data type that represents a graph.
#[derive(Debug)]
pub struct Graph {
    adj_list: FxHashMap<Node, Vec<Edge>>,
}

impl Graph {
    fn add_edge(&mut self, edge: Edge) {
        // Undirected graph is represented as 2 directed graphs!
        let reverse_edge = Edge { tail: edge.head, head: edge.tail, cost: edge.cost };
        self.adj_list.entry(edge.tail).or_default().push(edge);
        self.adj_list.entry(reverse_edge.tail).or_default().push(reverse_edge);
    }

    pub fn new(edges: Vec<Edge>) -> Self {
        let mut graph = Self { adj_list: FxHashMap::default() };
        for edge in edges {
            graph.add_edge(edge);
        }
        graph
    }

    fn parse_line(line: &str) -> Result<Edge> {
        let mut parts = line.split_whitespace();

        let tail_str = parts.next().ok_or(anyhow!("Missing tail node"))?;
        let head_str = parts.next().ok_or(anyhow!("Missing head node"))?;
        let cost_str = parts.next().ok_or(anyhow!("Missing edge cost"))?;

        let tail: Node = tail_str.parse()?;
        let head: Node = head_str.parse()?;
        let cost: Cost = cost_str.parse()?;

        Ok(Edge { tail, head, cost })
    }

    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<Self> {
        let file = File::open(path)?;
        let metadata = file.metadata()?;

        if metadata.len() == 0 {
            return Err(anyhow!("Cannot create graph from empty file"));
        }

        let mut lines = BufReader::new(file).lines().map(|l| l.unwrap());
        let first_line = lines.next().ok_or(anyhow!("Failed to read first line"))?;

        let mut parts = first_line.split_whitespace();
        let _ = parts.next().ok_or(anyhow!("Missing number of nodes"))?;
        let num_edges_str = parts.next().ok_or(anyhow!("Missing number of edges"))?;
        let num_edges: usize = num_edges_str.parse()?;

        let mut edges: Vec<Edge> = Vec::with_capacity(num_edges);

        for line in lines {
            let edge = Self::parse_line(&line)?;
            edges.push(edge);
        }

        Ok(Self::new(edges))
    }

    fn num_nodes(&self) -> usize {
        self.adj_list.len()
    }

    fn neighbors(&self, node: Node) -> impl Iterator<Item = &Edge> {
        self.adj_list.get(&node).unwrap().iter()
    }

    fn add_to_queue<'a>(
        &'a self,
        node: Node,
        queue: &mut BinaryHeap<&'a Edge>,
        visited: &mut FxHashSet<Node>,
    ) {
        visited.insert(node);
        for edge in self.neighbors(node).filter(|&e| !visited.contains(&e.head)) {
            queue.push(edge);
        }
    }

    /// Implementation of Prim's algorithm based on a heap that stores edges.
    /// This method assumes that a MST does exist.
    pub fn prim<'a>(&'a self, start: Node) -> (Self, Cost) {
        let num_edges = self.num_nodes() - 1;
        let mut edges: Vec<Edge> = Vec::with_capacity(num_edges);
        let mut mst_cost: Cost = 0;

        let mut queue: BinaryHeap<&'a Edge> = BinaryHeap::new();
        let mut visited: FxHashSet<Node> = FxHashSet::default();
        self.add_to_queue(start, &mut queue, &mut visited);

        while let Some(edge) = queue.pop() {
            let head = edge.head;
            if visited.contains(&head) {
                continue;
            }
            edges.push(edge.clone());
            mst_cost += edge.cost;
            self.add_to_queue(head, &mut queue, &mut visited);
        }

        (Self::new(edges), mst_cost)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_mst_cost_10_nodes() {
        let path = "text_files/prim_input_random_1_10.txt";
        let res = Graph::from_file(path);
        assert!(res.is_ok());

        let graph = res.unwrap();
        let start = 1;

        let (_, cost) = graph.prim(start);
        assert_eq!(cost, -7430);
    }

    #[test]
    fn test_mst_cost_20_nodes() {
        let path = "text_files/prim_input_random_5_20.txt";
        let res = Graph::from_file(path);
        assert!(res.is_ok());

        let graph = res.unwrap();
        let start = 1;

        let (_, cost) = graph.prim(start);
        assert_eq!(cost, -10519);
    }

    #[test]
    fn test_mst_cost_40_nodes() {
        let path = "text_files/prim_input_random_10_40.txt";
        let res = Graph::from_file(path);
        assert!(res.is_ok());

        let graph = res.unwrap();
        let start = 1;

        let (_, cost) = graph.prim(start);
        assert_eq!(cost, -97121);
    }

    #[test]
    fn test_mst_cost_80_nodes() {
        let path = "text_files/prim_input_random_15_80.txt";
        let res = Graph::from_file(path);
        assert!(res.is_ok());

        let graph = res.unwrap();
        let start = 1;

        let (_, cost) = graph.prim(start);
        assert_eq!(cost, -194537);
    }

    #[test]
    fn test_mst_cost_100_nodes() {
        let path = "text_files/prim_input_random_19_100.txt";
        let res = Graph::from_file(path);
        assert!(res.is_ok());

        let graph = res.unwrap();
        let start = 1;

        let (_, cost) = graph.prim(start);
        assert_eq!(cost, -217244);
    }
}
