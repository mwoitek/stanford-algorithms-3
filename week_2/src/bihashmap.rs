//! Very basic implementation of a bi-directional HashMap

use rustc_hash::{FxHashMap, FxHasher};
use std::collections::HashMap;
use std::hash::{BuildHasherDefault, Hash};
use std::rc::Rc;

#[derive(Debug, Default)]
pub struct BiHashMap<T: Eq + Hash> {
    forward: FxHashMap<usize, Rc<T>>,
    reverse: FxHashMap<Rc<T>, usize>,
}

impl<T: Eq + Hash> BiHashMap<T> {
    pub fn new() -> Self {
        Self { forward: FxHashMap::default(), reverse: FxHashMap::default() }
    }

    pub fn with_capacity(capacity: usize) -> Self {
        let hasher = BuildHasherDefault::<FxHasher>::default();
        Self {
            forward: HashMap::with_capacity_and_hasher(capacity, hasher.clone()),
            reverse: HashMap::with_capacity_and_hasher(capacity, hasher),
        }
    }

    pub fn insert(&mut self, key: usize, value: T) {
        let value_rc = Rc::new(value);
        self.forward.insert(key, Rc::clone(&value_rc));
        self.reverse.insert(Rc::clone(&value_rc), key);
    }

    pub fn get_forward(&self, key: &usize) -> Option<&T> {
        self.forward.get(key).map(|v| v.as_ref())
    }

    pub fn get_reverse(&self, key: &T) -> Option<&usize> {
        self.reverse.get(key)
    }

    pub fn contains_key_forward(&self, key: &usize) -> bool {
        self.forward.contains_key(key)
    }

    pub fn contains_key_reverse(&self, key: &T) -> bool {
        self.reverse.contains_key(key)
    }

    pub fn len(&self) -> usize {
        self.forward.len()
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
}
