//! Implementation of Union-Find data structure

use super::bihashmap::BiHashMap;
use anyhow::{anyhow, Result};
use std::hash::Hash;

/// Union-Find data structure.
#[derive(Debug, Default)]
pub struct UnionFind<T: Copy + Eq + Hash> {
    indices: BiHashMap<T>,
    parents: Vec<usize>,
    ranks: Vec<usize>,
}

impl<T: Copy + Eq + Hash> UnionFind<T> {
    pub fn new() -> Self {
        Self { indices: BiHashMap::new(), parents: Vec::new(), ranks: Vec::new() }
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            indices: BiHashMap::with_capacity(capacity),
            parents: Vec::with_capacity(capacity),
            ranks: Vec::with_capacity(capacity),
        }
    }

    /// Create a Union-Find from a list of elements.
    pub fn from_elements(elements: &[T]) -> Self {
        let mut uf = Self::with_capacity(elements.len());
        for &element in elements {
            uf.make_set(element);
        }
        uf
    }

    pub fn contains(&self, element: &T) -> bool {
        self.indices.contains_key_reverse(element)
    }

    /// Add an element to a new set.
    pub fn make_set(&mut self, element: T) {
        if self.contains(&element) {
            return;
        }
        let index = self.indices.len();
        self.indices.insert(index, element);
        self.parents.push(index);
        self.ranks.push(0);
    }

    // Implementation of the Find operation with path compression.
    // This version deals with the indices of the elements.
    fn find_index(&mut self, index: usize) -> usize {
        if self.parents[index] == index {
            return index;
        }
        self.parents[index] = self.find_index(self.parents[index]);
        self.parents[index]
    }

    /// Perform the Find operation.
    pub fn find(&mut self, element: &T) -> Option<&T> {
        let index = self.indices.get_reverse(element)?;
        let parent = self.find_index(*index);
        self.indices.get_forward(&parent)
    }

    /// Check if two elements belong to the same set.
    pub fn same_set(&mut self, elem_1: &T, elem_2: &T) -> bool {
        let index_1 = self.indices.get_reverse(elem_1);
        if index_1.is_none() {
            return false;
        }
        let parent_1 = self.find_index(*index_1.unwrap());

        let index_2 = self.indices.get_reverse(elem_2);
        if index_2.is_none() {
            return false;
        }
        let parent_2 = self.find_index(*index_2.unwrap());

        parent_1 == parent_2
    }

    // Implementation of the Union operation. This version uses union by rank, and
    // deals with the indices of the elements.
    fn union_index(&mut self, index_1: usize, index_2: usize) {
        let mut p1 = self.find_index(index_1);
        let mut p2 = self.find_index(index_2);

        if p1 == p2 {
            return;
        }

        if self.ranks[p1] < self.ranks[p2] {
            (p1, p2) = (p2, p1);
        }

        self.parents[p2] = p1;
        if self.ranks[p1] == self.ranks[p2] {
            self.ranks[p1] += 1;
        }
    }

    /// Perform the Union operation.
    pub fn union(&mut self, elem_1: &T, elem_2: &T) -> Result<()> {
        let index_1 = self
            .indices
            .get_reverse(elem_1)
            .ok_or_else(|| anyhow!("There is no set containing the 1st element"))?;
        let index_2 = self
            .indices
            .get_reverse(elem_2)
            .ok_or_else(|| anyhow!("There is no set containing the 2nd element"))?;
        self.union_index(*index_1, *index_2);
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_elements() {
        let elements = vec![1, 2, 3];
        let uf = UnionFind::from_elements(&elements);

        let num_elements = elements.len();
        assert_eq!(uf.indices.len(), num_elements);
        assert_eq!(uf.parents.len(), num_elements);
        assert_eq!(uf.ranks.len(), num_elements);

        for (i, element) in elements.iter().enumerate() {
            assert_eq!(uf.indices.get_reverse(element), Some(&i));
        }

        assert_eq!(uf.parents, (0..num_elements).collect::<Vec<_>>());
        assert_eq!(uf.ranks, vec![0; num_elements]);
    }

    #[test]
    fn test_find() {
        let elements = vec![1, 2, 3];
        let mut uf = UnionFind::from_elements(&elements);
        assert_eq!(uf.find(&1), Some(&1));
        assert_eq!(uf.find(&2), Some(&2));
        assert_eq!(uf.find(&3), Some(&3));
        assert_eq!(uf.find(&4), None);
    }

    #[test]
    fn test_same_set() {
        let mut uf = UnionFind::new();
        uf.make_set("Alice");
        uf.make_set("Bob");

        uf.union(&"Alice", &"Bob").unwrap();

        assert!(uf.same_set(&"Alice", &"Bob"));
        assert!(!uf.same_set(&"Alice", &"Chuck"));
        assert!(!uf.same_set(&"Danny", &"Bob"));
        assert!(!uf.same_set(&"Emma", &"Frank"));
    }

    #[test]
    fn test_union() {
        let elements = vec!['a', 'b', 'c'];
        let mut uf = UnionFind::from_elements(&elements);

        uf.union(&'a', &'b').unwrap();
        assert!(uf.same_set(&'a', &'b'));

        uf.union(&'b', &'c').unwrap();
        assert!(uf.same_set(&'a', &'c'));
    }

    #[test]
    fn test_union_errors() {
        let mut uf = UnionFind::from_elements(&[1, 2]);
        assert!(uf.union(&1, &3).is_err());
        assert!(uf.union(&3, &1).is_err());
        assert!(uf.union(&3, &4).is_err());
    }
}
