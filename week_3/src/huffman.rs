//! Week 3: Programming Assignment
//!
//! Huffman coding

use anyhow::{anyhow, Result};
use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::Add;
use std::path::Path;

// Node of a Huffman tree.
#[derive(Debug, Eq, PartialEq)]
enum Node {
    Internal { frequency: u64, left_child: Box<Node>, right_child: Box<Node> },
    Leaf { frequency: u64 },
}

impl Node {
    // Helper function to easily get the frequency of a node.
    fn frequency(&self) -> u64 {
        match self {
            Self::Internal { frequency, .. } => *frequency,
            Self::Leaf { frequency } => *frequency,
        }
    }
}

// Nodes will be added to a BinaryHeap. For this reason, the following traits are required.
impl Ord for Node {
    fn cmp(&self, other: &Self) -> Ordering {
        other.frequency().cmp(&self.frequency())
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

// To combine two nodes, + will be used.
impl Add for Node {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Node::Internal {
            frequency: self.frequency() + other.frequency(),
            left_child: Box::new(self),
            right_child: Box::new(other),
        }
    }
}

// Huffman tree.
#[derive(Debug)]
struct HuffmanTree {
    root: Node,
}

impl HuffmanTree {
    // Function to read the symbol frequencies. Returns a min heap containing leaf nodes.
    fn read_frequencies<P: AsRef<Path>>(path: P) -> Result<BinaryHeap<Node>> {
        let file = File::open(path)?;
        let mut lines = BufReader::new(file).lines().map(|l| l.unwrap());
        let _ = lines.next().ok_or(anyhow!("Failed to read number of symbols"))?;
        let mut leaves: BinaryHeap<Node> = BinaryHeap::new();
        for line in lines {
            let frequency: u64 = line.trim().parse()?;
            leaves.push(Node::Leaf { frequency });
        }
        Ok(leaves)
    }

    // Function to build a Huffman tree.
    fn new(heap: &mut BinaryHeap<Node>) -> Result<Self> {
        if heap.is_empty() {
            return Err(anyhow!("Cannot build tree from empty heap"));
        }
        while heap.len() > 1 {
            let node_1 = heap.pop().unwrap();
            let node_2 = heap.pop().unwrap();
            heap.push(node_1 + node_2);
        }
        let root = heap.pop().unwrap();
        Ok(Self { root })
    }

    /// Function to read an input file, and build a Huffman tree.
    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<Self> {
        let mut heap = Self::read_frequencies(path)?;
        Self::new(&mut heap)
    }

    // Recursive function to generate the codewords.
    fn codes_rec(node: &Node, code: String, codes: &mut Vec<String>) {
        match node {
            Node::Internal { left_child, right_child, .. } => {
                Self::codes_rec(left_child, code.clone() + "0", codes);
                Self::codes_rec(right_child, code + "1", codes);
            }
            Node::Leaf { .. } => {
                codes.push(code);
            }
        }
    }

    /// Function to generate all the codewords.
    pub fn codes(&self) -> Vec<String> {
        let mut codes: Vec<String> = Vec::new();
        Self::codes_rec(&self.root, "".to_owned(), &mut codes);
        codes
    }
}

/// Function to compute the maximum and minimum codeword lengths.
pub fn max_min_lengths<P: AsRef<Path>>(path: P) -> Result<(usize, usize)> {
    let tree = HuffmanTree::from_file(path)?;
    let codes = tree.codes();

    let lengths: Vec<usize> = codes.into_iter().map(|s| s.len()).collect();
    let max_length = *lengths.iter().max().unwrap();
    let min_length = *lengths.iter().min().unwrap();

    Ok((max_length, min_length))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_codes_4_symbols() {
        let path = "text_files/four_symbols.txt";
        let res = HuffmanTree::from_file(path);
        assert!(res.is_ok());

        let tree = res.unwrap();
        let codes = tree.codes();
        assert_eq!(codes.len(), 4);

        let expected_codes = ["0".to_owned(), "100".to_owned(), "101".to_owned(), "11".to_owned()];
        assert!(expected_codes.iter().all(|c| codes.contains(c)));
    }

    #[test]
    fn test_codes_5_symbols() {
        let path = "text_files/five_symbols.txt";
        let res = HuffmanTree::from_file(path);
        assert!(res.is_ok());

        let tree = res.unwrap();
        let codes = tree.codes();
        assert_eq!(codes.len(), 5);

        let expected_codes =
            ["00".to_owned(), "010".to_owned(), "011".to_owned(), "10".to_owned(), "11".to_owned()];
        assert!(expected_codes.iter().all(|c| codes.contains(c)));
    }

    #[test]
    fn test_lengths_10_symbols() {
        let path = "text_files/input_random_2_10.txt";
        let res = max_min_lengths(path);
        assert!(res.is_ok());
        let (max_length, min_length) = res.unwrap();
        assert_eq!(max_length, 4);
        assert_eq!(min_length, 3);
    }

    #[test]
    fn test_lengths_40_symbols() {
        let path = "text_files/input_random_10_40.txt";
        let res = max_min_lengths(path);
        assert!(res.is_ok());
        let (max_length, min_length) = res.unwrap();
        assert_eq!(max_length, 9);
        assert_eq!(min_length, 4);
    }

    #[test]
    fn test_lengths_160_symbols() {
        let path = "text_files/input_random_17_160.txt";
        let res = max_min_lengths(path);
        assert!(res.is_ok());
        let (max_length, min_length) = res.unwrap();
        assert_eq!(max_length, 13);
        assert_eq!(min_length, 6);
    }

    #[test]
    fn test_lengths_640_symbols() {
        let path = "text_files/input_random_25_640.txt";
        let res = max_min_lengths(path);
        assert!(res.is_ok());
        let (max_length, min_length) = res.unwrap();
        assert_eq!(max_length, 16);
        assert_eq!(min_length, 8);
    }

    #[test]
    fn test_lengths_2000_symbols() {
        let path = "text_files/input_random_33_2000.txt";
        let res = max_min_lengths(path);
        assert!(res.is_ok());
        let (max_length, min_length) = res.unwrap();
        assert_eq!(max_length, 20);
        assert_eq!(min_length, 10);
    }

    #[test]
    fn test_lengths_8000_symbols() {
        let path = "text_files/input_random_41_8000.txt";
        let res = max_min_lengths(path);
        assert!(res.is_ok());
        let (max_length, min_length) = res.unwrap();
        assert_eq!(max_length, 23);
        assert_eq!(min_length, 12);
    }

    #[test]
    fn test_lengths_10000_symbols() {
        let path = "text_files/input_random_48_10000.txt";
        let res = max_min_lengths(path);
        assert!(res.is_ok());
        let (max_length, min_length) = res.unwrap();
        assert_eq!(max_length, 26);
        assert_eq!(min_length, 12);
    }
}
