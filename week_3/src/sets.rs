//! Week 3: Programming Assignment
//!
//! Maximum-weight independent set of a path graph

use anyhow::{anyhow, Result};
use std::collections::HashSet;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

// Function to read the weights of the vertices in a path graph.
fn read_weights<P: AsRef<Path>>(path: P) -> Result<Vec<u64>> {
    let file = File::open(path)?;
    let mut lines = BufReader::new(file).lines().map(|l| l.unwrap());

    let num_vertices_str = lines.next().ok_or(anyhow!("Failed to read number of vertices"))?;
    let num_vertices: usize = num_vertices_str.trim().parse()?;

    let mut weights: Vec<u64> = Vec::with_capacity(num_vertices);
    for line in lines {
        let weight: u64 = line.trim().parse()?;
        weights.push(weight);
    }
    Ok(weights)
}

// Function that uses dynamic programming to compute the maximum weights.
fn max_weights(vertex_weights: &[u64]) -> Vec<u64> {
    let num_vertices = vertex_weights.len();
    let mut dp: Vec<u64> = Vec::with_capacity(num_vertices + 1);
    dp.push(0);

    if num_vertices == 0 {
        return dp;
    }

    dp.push(vertex_weights[0]);

    for i in 2..=num_vertices {
        let vertex_weight = vertex_weights[i - 1];
        let max_weight = dp[i - 1].max(dp[i - 2] + vertex_weight);
        dp.push(max_weight);
    }

    dp
}

// Function to construct the maximum-weight set from the DP solutions.
fn max_weight_set(max_weights: &[u64]) -> Result<HashSet<usize>> {
    if max_weights.is_empty() {
        return Err(anyhow!("Input size must be >= 1"));
    }

    let mut set: HashSet<usize> = HashSet::new();
    let mut i = max_weights.len() - 1;

    while i >= 1 {
        if max_weights[i] == max_weights[i - 1] {
            i -= 1;
        } else {
            set.insert(i);
            i = i.saturating_sub(2);
        }
    }

    Ok(set)
}

/// Function to generate the assignment answer.
pub fn check_membership<P: AsRef<Path>>(path: P, vertices: &[usize]) -> Result<String> {
    let vertex_weights = read_weights(path)?;
    let dp = max_weights(&vertex_weights);
    let set = max_weight_set(&dp)?;

    let mut answer = String::with_capacity(vertices.len());

    for vertex in vertices {
        let b = if set.contains(vertex) { '1' } else { '0' };
        answer.push(b);
    }

    Ok(answer)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_10_vertices() {
        let path = "text_files/problem16.6test.txt";
        let res_1 = read_weights(path);
        assert!(res_1.is_ok());

        let vertex_weights = res_1.unwrap();
        let dp = max_weights(&vertex_weights);
        assert_eq!(dp.last(), Some(&2617));

        let res_2 = max_weight_set(&dp);
        assert!(res_2.is_ok());

        let set = res_2.unwrap();
        let expected_set = [2, 4, 7, 10];
        assert_eq!(set.len(), expected_set.len());
        assert!(expected_set.iter().all(|v| set.contains(v)));
    }

    #[test]
    fn test_membership_str_40_vertices() {
        let path = "text_files/mwis_input_random_10_40.txt";
        let vertices = [1, 2, 3, 4, 17, 117, 517, 997];

        let res = check_membership(path, &vertices);
        assert!(res.is_ok());

        let answer = res.unwrap();
        assert_eq!(answer, "10010000".to_owned());
    }

    #[test]
    fn test_membership_str_160_vertices() {
        let path = "text_files/mwis_input_random_17_160.txt";
        let vertices = [1, 2, 3, 4, 17, 117, 517, 997];

        let res = check_membership(path, &vertices);
        assert!(res.is_ok());

        let answer = res.unwrap();
        assert_eq!(answer, "10100100".to_owned());
    }

    #[test]
    fn test_membership_str_640_vertices() {
        let path = "text_files/mwis_input_random_26_640.txt";
        let vertices = [1, 2, 3, 4, 17, 117, 517, 997];

        let res = check_membership(path, &vertices);
        assert!(res.is_ok());

        let answer = res.unwrap();
        assert_eq!(answer, "10100110".to_owned());
    }

    #[test]
    fn test_membership_str_2000_vertices() {
        let path = "text_files/mwis_input_random_33_2000.txt";
        let vertices = [1, 2, 3, 4, 17, 117, 517, 997];

        let res = check_membership(path, &vertices);
        assert!(res.is_ok());

        let answer = res.unwrap();
        assert_eq!(answer, "10010110".to_owned());
    }

    #[test]
    fn test_membership_str_4000_vertices() {
        let path = "text_files/mwis_input_random_39_4000.txt";
        let vertices = [1, 2, 3, 4, 17, 117, 517, 997];

        let res = check_membership(path, &vertices);
        assert!(res.is_ok());

        let answer = res.unwrap();
        assert_eq!(answer, "10010010".to_owned());
    }

    #[test]
    fn test_membership_str_10000_vertices() {
        let path = "text_files/mwis_input_random_47_10000.txt";
        let vertices = [1, 2, 3, 4, 17, 117, 517, 997];

        let res = check_membership(path, &vertices);
        assert!(res.is_ok());

        let answer = res.unwrap();
        assert_eq!(answer, "10100000".to_owned());
    }
}
