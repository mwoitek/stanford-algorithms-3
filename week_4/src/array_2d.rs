//! Simple implementation of a 2D array

use anyhow::{anyhow, Result};

pub struct Array2D<T> {
    rows: usize,
    cols: usize,
    data: Vec<Vec<T>>,
}

impl<T> Array2D<T> {
    /// Create an Array2D with given dimensions and initial value.
    pub fn new(rows: usize, cols: usize, initial: T) -> Self
    where
        T: Clone,
    {
        Self { rows, cols, data: vec![vec![initial; cols]; rows] }
    }

    /// Get the element at (row, col).
    pub fn get(&self, row: usize, col: usize) -> Option<&T> {
        if row < self.rows && col < self.cols {
            Some(&self.data[row][col])
        } else {
            None
        }
    }

    /// Set the element at (row, col).
    pub fn set(&mut self, row: usize, col: usize, value: T) -> Result<()> {
        if row < self.rows && col < self.cols {
            self.data[row][col] = value;
            Ok(())
        } else {
            Err(anyhow!("Index out of bounds"))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_create_array() {
        let array = Array2D::new(2, 3, 0);
        assert_eq!(array.get(0, 0), Some(&0));
        assert_eq!(array.get(1, 2), Some(&0));
        assert_eq!(array.get(2, 0), None); // Out of bounds
        assert_eq!(array.get(0, 3), None); // Out of bounds
    }

    #[test]
    fn test_initial_value() {
        let array = Array2D::new(2, 2, 1);
        assert_eq!(array.get(0, 0), Some(&1));
        assert_eq!(array.get(1, 1), Some(&1));
    }

    #[test]
    fn test_set_and_get_element() {
        let mut array = Array2D::new(3, 3, 0);
        assert!(array.set(1, 1, 5).is_ok());
        assert_eq!(array.get(1, 1), Some(&5));
    }

    #[test]
    fn test_out_of_bounds() {
        let mut array = Array2D::new(2, 2, 1);
        assert_eq!(array.get(2, 0), None);
        assert_eq!(array.get(0, 2), None);

        let res = array.set(2, 0, 5);
        assert!(res.is_err());
        assert_eq!(res.unwrap_err().to_string(), "Index out of bounds".to_string());

        let res = array.set(0, 2, 5);
        assert!(res.is_err());
        assert_eq!(res.unwrap_err().to_string(), "Index out of bounds".to_string());
    }
}
