//! Week 4: Programming Assignment
//!
//! 0-1 Knapsack Problem

pub mod array_2d;

use anyhow::{anyhow, Result};
use array_2d::Array2D;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

/// Data type that represents a knapsack item.
#[derive(Debug)]
pub struct KSItem {
    value: usize,
    weight: usize,
}

// Helper function to parse a line of an input file, and get the knapsack item.
fn parse_line(line: &str) -> Result<KSItem> {
    let mut parts = line.split_whitespace();

    let value_str = parts.next().ok_or(anyhow!("Missing item value"))?;
    let weight_str = parts.next().ok_or(anyhow!("Missing item weight"))?;

    let value: usize = value_str.parse()?;
    let weight: usize = weight_str.parse()?;

    Ok(KSItem { value, weight })
}

/// Function for reading an input file containing data related to the 0-1 Knapsack Problem.
pub fn read_input<P: AsRef<Path>>(path: P) -> Result<(Vec<KSItem>, usize)> {
    let file = File::open(path)?;
    let mut lines = BufReader::new(file).lines().map(|l| l.unwrap());

    let first_line = lines.next().ok_or(anyhow!("Failed to read first line"))?;
    let mut parts = first_line.split_whitespace();

    let capacity_str = parts.next().ok_or(anyhow!("Missing knapsack size"))?;
    let num_items_str = parts.next().ok_or(anyhow!("Missing number of items"))?;

    let capacity: usize = capacity_str.parse()?;
    let num_items: usize = num_items_str.parse()?;

    let mut items: Vec<KSItem> = Vec::with_capacity(num_items);

    for line in lines {
        let item = parse_line(&line)?;
        items.push(item);
    }

    Ok((items, capacity))
}

/// Dynamic programming solution to the 0-1 Knapsack Problem.
pub fn knapsack(items: &[KSItem], capacity: usize) -> usize {
    let num_items = items.len();
    let mut max_val = Array2D::new(num_items + 1, capacity + 1, 0_usize);

    for i in 1..=num_items {
        let diff = i - 1;
        let (value, weight) = (items[diff].value, items[diff].weight);

        for j in 1..=capacity {
            let m1 = *max_val.get(diff, j).unwrap();

            if weight > j {
                let _ = max_val.set(i, j, m1);
            } else {
                let m2 = max_val.get(diff, j - weight).unwrap() + value;
                let _ = max_val.set(i, j, m1.max(m2));
            }
        }
    }

    *max_val.get(num_items, capacity).unwrap()
}

/// Solution to the 0-1 Knapsack Problem that uses less memory.
pub fn knapsack_optimized(items: &[KSItem], capacity: usize) -> usize {
    let mut max_val = vec![0_usize; capacity + 1];

    for item in items {
        let (value, weight) = (item.value, item.weight);
        for j in (1..=capacity).rev().filter(|&x| x >= weight) {
            max_val[j] = max_val[j].max(max_val[j - weight] + value);
        }
    }

    *max_val.last().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_knapsack_10_items_capacity_10() {
        let path = "text_files/input_random_5_10_10.txt";
        let res = read_input(path);
        assert!(res.is_ok());
        let (items, capacity) = res.unwrap();
        let max_val = knapsack(&items, capacity);
        assert_eq!(max_val, 14);
    }

    #[test]
    fn test_knapsack_10_items_capacity_100() {
        let path = "text_files/input_random_10_100_10.txt";
        let res = read_input(path);
        assert!(res.is_ok());
        let (items, capacity) = res.unwrap();
        let max_val = knapsack(&items, capacity);
        assert_eq!(max_val, 147);
    }

    #[test]
    fn test_knapsack_100_items_capacity_100() {
        let path = "text_files/input_random_13_100_100.txt";
        let res = read_input(path);
        assert!(res.is_ok());
        let (items, capacity) = res.unwrap();
        let max_val = knapsack(&items, capacity);
        assert_eq!(max_val, 529);
    }

    #[test]
    fn test_knapsack_1000_items_capacity_100() {
        let path = "text_files/input_random_17_100_1000.txt";
        let res = read_input(path);
        assert!(res.is_ok());
        let (items, capacity) = res.unwrap();
        let max_val = knapsack(&items, capacity);
        assert_eq!(max_val, 1993);
    }

    #[test]
    fn test_knapsack_optimized_1000_items_capacity_100() {
        let path = "text_files/input_random_17_100_1000.txt";
        let res = read_input(path);
        assert!(res.is_ok());
        let (items, capacity) = res.unwrap();
        let max_val = knapsack_optimized(&items, capacity);
        assert_eq!(max_val, 1993);
    }

    #[test]
    fn test_knapsack_optimized_1000_items_capacity_1000() {
        let path = "text_files/input_random_25_1000_1000.txt";
        let res = read_input(path);
        assert!(res.is_ok());
        let (items, capacity) = res.unwrap();
        let max_val = knapsack_optimized(&items, capacity);
        assert_eq!(max_val, 18604);
    }

    #[test]
    fn test_knapsack_optimized_1000_items_capacity_10000() {
        let path = "text_files/input_random_29_10000_1000.txt";
        let res = read_input(path);
        assert!(res.is_ok());
        let (items, capacity) = res.unwrap();
        let max_val = knapsack_optimized(&items, capacity);
        assert_eq!(max_val, 167375);
    }

    #[test]
    fn test_knapsack_optimized_2000_items_capacity_100000() {
        let path = "text_files/input_random_33_100000_2000.txt";
        let res = read_input(path);
        assert!(res.is_ok());
        let (items, capacity) = res.unwrap();
        let max_val = knapsack_optimized(&items, capacity);
        assert_eq!(max_val, 2688014);
    }

    #[ignore]
    #[test]
    fn test_knapsack_optimized_2000_items_capacity_1000000() {
        let path = "text_files/input_random_37_1000000_2000.txt";
        let res = read_input(path);
        assert!(res.is_ok());
        let (items, capacity) = res.unwrap();
        let max_val = knapsack_optimized(&items, capacity);
        assert_eq!(max_val, 25178376);
    }

    #[ignore]
    #[test]
    fn test_knapsack_optimized_2000_items_capacity_2000000() {
        let path = "text_files/input_random_44_2000000_2000.txt";
        let res = read_input(path);
        assert!(res.is_ok());
        let (items, capacity) = res.unwrap();
        let max_val = knapsack_optimized(&items, capacity);
        assert_eq!(max_val, 49957183);
    }
}
